#include "HTTPController.h"
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QDebug>
#include <QEventLoop>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>

HttpController::HttpController(QObject *main) : showText(main)
{
    nam = new QNetworkAccessManager(this);
    connect(nam, &QNetworkAccessManager::finished, this, &HttpController::doQml);
}

void HttpController::getNetworkValue()
{
    QEventLoop evntLoop;
    QNetworkRequest request;
    connect(nam, &QNetworkAccessManager::finished, &evntLoop, &QEventLoop::quit);
    request.setUrl(QUrl("https://music.yandex.ru/artist/75628"));
    QNetworkReply * reply = nam->get(request);
    evntLoop.exec();
    QString replyString = reply->readAll();
}

void HttpController::doQml(QNetworkReply *message){
    //qDebug() << "QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQq";
    //qDebug() << message->readAll();
    QString str = message->readAll();
    QObject* textRich = showText->findChild<QObject*>("textRich");
    QObject* textRaw = showText->findChild<QObject*>("textRaw");
    QObject* textField = showText->findChild<QObject*>("textField");
    textRaw->setProperty("text", str);
    textRich->setProperty("text", str);
    textField->setProperty("text", str.mid(str.indexOf("d-button__label") + 17, str.mid(str.indexOf("d-button__label") + 17, 60).indexOf("</span>")));
}

