import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.3
import QtMultimedia 5.15
import QtGraphicalEffects 1.15

Page {
    id: page3GUI
    width: 600
    height: 900
    visible: true
    header:
        GridLayout{
        columnSpacing: 10
        anchors.rightMargin: 15
        anchors.bottomMargin: 0
        anchors.leftMargin: 15
        anchors.topMargin: 15
        Image {
            id: googleImg3
            source: "/unnamed.png"
            Layout.column: 1
            Layout.row: 1
        }
        Label {
            id: labLable3
            text: qsTr("ЛР3 HTTP-запросы")
            font.pixelSize: 20
            Layout.column: 2
            Layout.row: 1
            font.family: 'Arial'
        }
    }

    Rectangle{
        id: hr3
        color:"#DFE1E5"
        y:0
        height:1
        width: parent.width
    }

    GridLayout {
        anchors.fill: parent
        anchors.margins: 15

        Button{
            id: sent
            Layout.alignment: Qt.AlignCenter
            Layout.minimumWidth: 100
            Layout.minimumHeight: 60
            Layout.row: 1
            Layout.column: 1
            onClicked: {
                app.btnHttpRequest()
            }
            background: Rectangle {
                width: parent.width
                height: parent.height
                color: sent.pressed ?  "#DFE1E5": "#F8F9FA"
                radius: 4
            }
            Text {
                text: qsTr("Отправить")
                anchors.centerIn: parent
                font.family: 'Arial'
                font.pixelSize: 14
            }
        }
        RowLayout{
            id:rowRadioHttp
            Layout.alignment: Qt.AlignCenter
            Layout.column: 1
            Layout.row: 2
            anchors.top: sent.bottom
            anchors.topMargin: 10
            RadioButton {
                id:vidBtn
                checked: true
                text: qsTr("Rich")
                onClicked: {
                    richText.visible = true
                    rawText.visible  = false
                }
            }
            RadioButton {
                id:camBtn
                text: qsTr("Raw")
                onClicked: {
                    richText.visible = false
                    rawText.visible  = true
                }
            }
        }

        ScrollView {
            id: richText
            Layout.fillHeight: true
            Layout.fillWidth: true
            clip:  true
            Layout.row: 3
            Layout.column: 1
            Text{
                //anchors.margins: 15
                id: text1
                textFormat: Text.RichText
                objectName: "textRich"
                anchors.fill: parent
                color: 'black'
            }
        }
        ScrollView {
            id: rawText
            Layout.fillHeight: true
            Layout.fillWidth: true
            clip:  true
            Layout.row: 3
            Layout.column: 1
            visible: false
            Text{
                //anchors.margins: 15
                id: text2
                textFormat: Text.PlainText
                objectName: "textRaw"
                anchors.fill: parent
                color: 'black'
            }
        }
        Text{
            text: qsTr("Данный инсполнитель нравится")
            Layout.alignment: Qt.AlignHCenter
            font.family: 'Arial'
            font.pixelSize: 14
            Layout.row: 4
            Layout.column: 1
            anchors.bottom: textField.top
            anchors.bottomMargin: 3
        }
        TextField {
            id: textField
            anchors.bottom: parent.bottom
            //anchors.bottomMargin: 3
            Layout.column: 1
            Layout.row: 5
            objectName: "textField"
            color: "black"
            horizontalAlignment: Text.AlignHCenter
            readOnly: true
            Layout.alignment: Qt.AlignCenter
            width: parent.width / 3
            Layout.minimumWidth: 150
            font.family: 'Arial'
            font.pixelSize: 14
            background: Rectangle {
                width: parent.width
                height: parent.height
                border.color: textField.activeFocus ?  "#4285F4": "#DFE1E5"
                border.width: 2
                radius: 4
            }
        }
        Text{
            text: qsTr("людям")
            Layout.alignment: Qt.AlignHCenter
            font.family: 'Arial'
            font.pixelSize: 14
            Layout.row: 6
            Layout.column: 1
            anchors.top: textField.bottom
        }
    }
}
