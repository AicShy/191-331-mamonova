#ifndef HTTPCONTROLLER_H
#define HTTPCONTROLLER_H

#include <QObject>
#include <QNetworkAccessManager>



class HttpController : public QObject

{
    Q_OBJECT
public:
    explicit HttpController(QObject *parent = nullptr);

    QNetworkAccessManager *nam = nullptr;

    ~HttpController() {
        delete nam;
    }

public slots:
    void getNetworkValue();
    void doQml(QNetworkReply *message);
signals:
    void btnHttpRequest();
protected:
     QObject * showText;
};

#endif // HTTPCONTROLLER_H
