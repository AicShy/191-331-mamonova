#include <QGuiApplication>
#include <QtMultimedia>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "HTTPController.h"


int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    const QUrl url(QStringLiteral("qrc:/main.qml"));                    //задание файлв QML-разметки для стартовой страницы окна приложеня

    QQmlApplicationEngine engine;                                       //создание объекта движка/интерпретатора QML

    QQmlContext *context = engine.rootContext();

    //Конструкция ниже задает связь между событием "objectCreaated" объекта "engine"
    //и коллбекомм

    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {    //коллбек на случай ошибки внутри движка, объявленн
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);                                 //действия на случай ошибки внутри движка
    }, Qt::QueuedConnection);

    engine.load(url);                                                   //загрузить файл стартовой страницы в движок

    QObject* main = engine.rootObjects()[0];

    HttpController text(main);
    HttpController::connect(main, SIGNAL(btnHttpRequest()), &text, SLOT(getNetworkValue()));

    return app.exec();                                                  //начало работы приложния, то есть передача управления
                                                                        //от точки входа коду самого приложения (cpp и QML)
}

/*строение проекта Qt QML:
 * .pro - файл настроек системы сборки qmake,
 *      все файлы из дерева проекта
 *      перечилены в *.pro, и при удалении из *.pro - файлы удаляются из дерева
 *      внешние библиотеки (.lib и .h-файлы) подключаются через *.pro
 *      различие процесса сборки для ОС задается в *.pro
 * main.cpp - точка входа в приложение. А в случае приложения QML в main создается
 * объект движка-интерпретатора QML-разметки
 *
 * Как и в любом с++ приложении, в проекте могут быть и другие cpp и h файлы
 *
 * Описание интерфейса приложения и простейших механик его логики содержится в
 * файлах QML, которые выполняют роль фронтэнда. QML - диалект JS + JSON
 *
 *Структура базовой функции main
 *
 *qrc - файл ресурсов, туда помещаются любые некомпелируемые данные,
 *изображения, 3D-сетки, адио и т.д.
 *нужно, чтобы не потерять
*/
