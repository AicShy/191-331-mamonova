import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.3
import QtMultimedia 5.15
import QtGraphicalEffects 1.15

ApplicationWindow {
    id: app
    width: 600
    height: 900
    visible: true
    title: qsTr("Tabs")
    signal btnHttpRequest()

    SwipeView {
        id: swipeView
        anchors.fill: parent
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0
        currentIndex: tabBar.currentIndex


        Page1GUI {
            id: page1GUI
        }

        Page2GUI {
            id: page2GUI
        }

        Page3GUI {
            id: page3GUI
        }
        Page4GUI {
            id: page4GUI
        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex

        TabButton {
            x: 0
            y: 0
            text: qsTr("Page 1")
        }
        TabButton {
            x: 96
            y: 0
            text: qsTr("Page 2")
        }
        TabButton {
            x: 192
            y: 0
            text: qsTr("Page 3")
        }
        TabButton {
            x: 192
            y: 0
            text: qsTr("Extra")
        }
    }
}

/*##^##
Designer {
    D{i:0;formeditorZoom:0.66}D{i:2;invisible:true}
}
##^##*/
