import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.3
import QtMultimedia 5.15
import QtGraphicalEffects 1.15

Page {
    id : page1GUI
    header:
        GridLayout{
        columnSpacing: 10
        anchors.rightMargin: 15
        anchors.bottomMargin: 15
        anchors.leftMargin: 15
        anchors.topMargin: 15
        Image {
            id: googleImg
            source: "/unnamed.png"
            Layout.column: 1
            Layout.row: 1
        }
        Label {
            id: labLable
            text: qsTr("ЛР1 Элементы GUI")
            font.pixelSize: 20
            Layout.column: 2
            Layout.row: 1
            font.family: 'Arial'
        }
    }

    Rectangle{
        id: hr
        color:"#DFE1E5"
        y:0
        height:1
        width: parent.width
    }

    GridLayout{
        anchors.fill: parent
        anchors.rightMargin: 15
        anchors.bottomMargin: 15
        anchors.leftMargin: 15
        anchors.topMargin: 15
        columnSpacing: 10
        rowSpacing: 10

        Image{
            id: googleImgBIG
            source: "googleBig.png"
            Layout.columnSpan: 2
            Layout.row: 0
            Layout.maximumWidth: 300
            Layout.maximumHeight: 101
            anchors.horizontalCenter: parent.horizontalCenter
        }
        Column{
            id: searchBlock
            Layout.preferredHeight: 100
            Layout.columnSpan: 2
            Layout.rowSpan: 1
            Layout.row: 1
            Layout.fillWidth: true
            GridLayout{
                anchors.fill: parent
                anchors.rightMargin: 0
                anchors.bottomMargin: 0
                anchors.leftMargin: 0
                anchors.topMargin: 0
                Rectangle{
                    id: searchOutline
                    border.color: "#DFE1E5"
                    border.width: 1
                    radius: 15
                    Layout.columnSpan: 2
                    Layout.row: 0
                    Layout.preferredWidth: parent.width
                    Layout.preferredHeight: 30
                    anchors.horizontalCenter: parent.horizontalCenter
                    TextInput {
                        width: parent.width - searchIco.width - 26
                        height: parent.height - 16
                        id: textInput
                        font.pixelSize: 14
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.right: searchIco.left
                        anchors.left: searchOutline.left
                        anchors.rightMargin: 13
                        anchors.leftMargin: 13
                        clip: true
                        font.family: 'Arial'
                    }
                    Image {
                        id: searchIco
                        source: "/search.svg"
                        anchors.right: searchOutline.right
                        anchors.verticalCenter: parent.verticalCenter
                        height: parent.height/2
                        width: parent.height/2
                        anchors.rightMargin: 10
                    }
                }

                Row{
                    id: btnBlock
                    Layout.row: 1
                    Layout.columnSpan: 2
                    Layout.fillWidth: true
                    anchors.horizontalCenter: parent.horizontalCenter
                    Layout.preferredHeight: 30
                    GridLayout{
                        anchors.fill: parent
                        anchors.rightMargin: 40
                        anchors.bottomMargin: 0
                        anchors.leftMargin: 40
                        anchors.topMargin: 0
                        columnSpacing: 10

                        Button{
                            id: searchBtn
                            Layout.minimumWidth: 100
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                            Layout.column: 0
                            background: Rectangle {
                                width: parent.width
                                height: parent.height
                                color: searchBtn.pressed ?  "#DFE1E5": "#F8F9FA"
                                radius: 4
                            }
                            Text {
                                text: qsTr("Поиск в Google")
                                anchors.centerIn: parent
                                font.family: 'Arial'
                                font.pixelSize: 14
                            }
                        }

                        Button{
                            id: luckyBtn
                            Layout.minimumWidth: 100
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                            Layout.column: 1
                            background: Rectangle {
                                width: parent.width
                                height: parent.height
                                color: luckyBtn.pressed ?  "#DFE1E5": "#F8F9FA"
                                radius: 4
                            }
                            Text {
                                text: qsTr("Мне повезёт!")
                                anchors.centerIn: parent
                                font.family: 'Arial'
                                font.pixelSize: 14
                            }
                        }
                    }
                }
            }
        }

        Row {
            id: otherStaffBlock
            anchors.top: btnBlock.Bottom
            Layout.fillHeight: true
            Layout.columnSpan: 2
            Layout.row: 2
            Layout.fillWidth: true
            GridLayout{
                anchors.fill: parent
                anchors.rightMargin: 0
                anchors.bottomMargin: 0
                anchors.leftMargin: 0
                anchors.topMargin: 0
                columnSpacing: 10
                Column{
                    Layout.column: 1
                    Layout.row: 1
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    GridLayout{
                        anchors.fill: parent
                        anchors.rightMargin: 0
                        anchors.bottomMargin: 0
                        anchors.leftMargin: 0
                        anchors.topMargin: 0
                        Text {
                            id:loginText
                            text: qsTr("Введите логин")
                            Layout.column: 1
                            Layout.row: 1
                            font.family: 'Arial'
                            font.pixelSize: 14
                            wrapMode: Text.Wrap
                            Layout.fillWidth: true
                        }
                        TextField{
                            id: loginField
                            anchors.top: loginText.bottom
                            anchors.topMargin: 4
                            Layout.column: 1
                            Layout.row: 2
                            font.family: 'Arial'
                            font.pixelSize: 14
                            wrapMode: Text.Wrap
                            placeholderText: "Логин"
                            Layout.fillWidth: true
                            background: Rectangle {
                                width: parent.width
                                height: parent.height
                                border.color: loginField.activeFocus ?  "#4285F4": "#DFE1E5"
                                border.width: 2
                                radius: 4
                            }
                        }

                        Text {
                            id:emailText
                            text: qsTr("Введите E-mail")
                            anchors.top: loginField.bottom
                            anchors.topMargin: 20
                            Layout.column: 1
                            Layout.row: 3
                            font.family: 'Arial'
                            font.pixelSize: 14
                            wrapMode: Text.Wrap
                            Layout.fillWidth: true
                        }
                        TextField{
                            id: emailField
                            anchors.top: emailText.bottom
                            anchors.topMargin: 2
                            Layout.column: 1
                            Layout.row: 4
                            font.family: 'Arial'
                            font.pixelSize: 14
                            wrapMode: Text.Wrap
                            placeholderText: "E-mail"
                            Layout.fillWidth: true
                            background: Rectangle {
                                width: parent.width
                                height: parent.height
                                border.color: emailField.activeFocus ?  "#4285F4": "#DFE1E5"
                                border.width: 2
                                radius: 4
                            }
                        }

                        Text {
                            id:passText
                            text: qsTr("Введите пароль")
                            anchors.top: emailField.bottom
                            anchors.topMargin: 20
                            Layout.column: 1
                            Layout.row: 5
                            font.family: 'Arial'
                            font.pixelSize: 14
                            wrapMode: Text.Wrap
                            Layout.fillWidth: true
                        }
                        TextField{
                            id: passField
                            anchors.top: passText.bottom
                            anchors.topMargin: 2
                            Layout.column: 1
                            Layout.row: 6
                            font.family: 'Arial'
                            font.pixelSize: 14
                            wrapMode: Text.Wrap
                            placeholderText: "Пароль"
                            Layout.fillWidth: true
                            echoMode: TextInput.Password
                            background: Rectangle {
                                width: parent.width
                                height: parent.height
                                border.color: passField.activeFocus ?  "#4285F4": "#DFE1E5"
                                border.width: 2
                                radius: 4
                            }
                        }
                        Text {
                            id:passText2
                            text: qsTr("Повторите пароль")
                            anchors.top: passField.bottom
                            Layout.column: 1
                            Layout.row: 7
                            anchors.topMargin: 20
                            font.family: 'Arial'
                            font.pixelSize: 14
                            wrapMode: Text.Wrap
                            Layout.fillWidth: true
                        }
                        TextField{
                            id: passField2
                            anchors.top: passText2.bottom
                            anchors.topMargin: 2
                            Layout.column: 1
                            Layout.row: 8
                            font.family: 'Arial'
                            font.pixelSize: 14
                            wrapMode: Text.Wrap
                            placeholderText: "Повторите пароль"
                            Layout.fillWidth: true
                            echoMode: TextInput.Password
                            background: Rectangle {
                                width: parent.width
                                height: parent.height
                                border.color: passField2.activeFocus ?  "#4285F4": "#DFE1E5"
                                border.width: 2
                                radius: 4
                            }
                        }
                    }
                }
                Column{
                    id: tumblerBlock
                    Layout.column: 2
                    Layout.row: 1
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    GridLayout{
                        anchors.fill: parent
                        anchors.rightMargin: 0
                        anchors.bottomMargin: 0
                        anchors.leftMargin: 0
                        anchors.topMargin: 0

                        Text {
                            id: tumblerText
                            text: qsTr("Введите нынешнее время:")
                            Layout.column: 1
                            Layout.row: 1
                            font.family: 'Arial'
                            font.pixelSize: 14
                            wrapMode: Text.Wrap
                            Layout.fillWidth: true
                        }
                        Row{
                            id: tumbler
                            anchors.top: tumblerText.bottom
                            anchors.topMargin: 2
                            Layout.minimumHeight: 170
                            Layout.column: 1
                            Layout.row: 2
                            Layout.fillHeight: false
                            Layout.fillWidth: true
                            GridLayout{
                                anchors.top: parent.top
                                Tumbler {
                                    id: hoursTumbler
                                    Layout.fillHeight: true
                                    Layout.fillWidth: false
                                    model: 24
                                    delegate: delegateComponent
                                    wrap: true
                                    visibleItemCount:5
                                    Layout.column: 1
                                    Layout.row: 2
                                    Layout.preferredHeight: 150
                                    Layout.maximumWidth: 50
                                    Layout.minimumWidth: 30
                                }
                                Text {
                                    text: qsTr(":")
                                    height: 150
                                }
                                Tumbler {
                                    id: minutesTumbler
                                    Layout.fillWidth: false
                                    Layout.fillHeight: true
                                    model: 60
                                    delegate: delegateComponent
                                    wrap: true
                                    visibleItemCount:5
                                    Layout.column: 2
                                    Layout.row: 2
                                    Layout.preferredHeight: 150
                                    Layout.maximumWidth: 50
                                    Layout.minimumWidth: 30
                                }
                                Text {
                                    text: qsTr(":")
                                    height: 150
                                }
                                Tumbler {
                                    id: secondsTumbler
                                    Layout.fillWidth: false
                                    Layout.fillHeight: true
                                    model: 60
                                    delegate: delegateComponent
                                    wrap: true
                                    visibleItemCount:5
                                    Layout.column: 3
                                    Layout.row: 2
                                    Layout.preferredHeight: 150
                                    Layout.maximumWidth: 50
                                    Layout.minimumWidth: 30
                                }
                            }
                        }
                        Text{
                            id: sliderText
                            anchors.top: tumbler.bottom
                            anchors.topMargin: 20
                            text: qsTr("Выберите громкость")
                            Layout.column: 1
                            Layout.row: 3
                            font.family: 'Arial'
                            font.pixelSize: 14
                            wrapMode: Text.Wrap
                            Layout.fillWidth: true
                        }

                        Slider{
                            id: soundSlider
                            anchors.top: sliderText.bottom
                            anchors.topMargin: 2
                            Layout.fillWidth: true
                            Layout.column: 1
                            Layout.row: 4
                            background: Rectangle {
                                x: soundSlider.leftPadding
                                y: soundSlider.topPadding + soundSlider.availableHeight / 2 - height / 2
                                implicitHeight: 6
                                width: soundSlider.availableWidth
                                height: implicitHeight
                                radius: 3
                                color: "#D1D4D6"

                                Rectangle {
                                    width: soundSlider.visualPosition * parent.width
                                    height: parent.height
                                    color: "#4285F4"
                                    radius: 3
                                }
                            }

                            handle: Rectangle {
                                x: soundSlider.leftPadding + soundSlider.visualPosition * (soundSlider.availableWidth - width)
                                y: soundSlider.topPadding + soundSlider.availableHeight / 2 - height / 2
                                implicitWidth: 20
                                implicitHeight: 20
                                radius: 10
                                color: soundSlider.pressed ? "#EDEFF4": "#ffffff"
                                border.color: "#DFE1E5"
                            }
                        }
                    }
                }
            }
        }
    }
}
