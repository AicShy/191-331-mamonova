import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.3
import QtMultimedia 5.15
import QtGraphicalEffects 1.15
import QtQuick.Dialogs 1.3
//import Qt.labs.platform 1.1
//сохранение картинки!

Page{
    id: page4GUI
    width: 600
    height: 900
    visible: true
    header:
        GridLayout{
        columnSpacing: 10
        anchors.rightMargin: 15
        anchors.bottomMargin: 0
        anchors.leftMargin: 15
        anchors.topMargin: 15
        Image {
            id: googleImg4
            source: "/unnamed.png"
            Layout.column: 1
            Layout.row: 1
        }
        Label {
            id: labLable4
            text: qsTr("Работа с фото")
            font.pixelSize: 20
            Layout.column: 2
            Layout.row: 1
            font.family: 'Arial'
        }
    }

    Rectangle{
        id: hr4
        color:"#DFE1E5"
        y:0
        height:1
        width: parent.width
    }
    GridLayout{
        anchors.fill: parent
        anchors.rightMargin: 15
        anchors.bottomMargin: 15
        anchors.leftMargin: 15
        anchors.topMargin: 15
        columnSpacing: 10
        rowSpacing: 10

        RowLayout{
            id:rowRadioPhoto
            Layout.alignment: Qt.AlignCenter
            Layout.row: 1
            anchors.top: parent.top
            RadioButton {
                id:zoomBtn
                checked: true
                text: qsTr("Zoom\nBlur")
                Layout.alignment: Qt.AlignVCenter
                onClicked: {
                    zoom.visible = true
                    shadow.visible = false
                    hue.visible  = false
                }
            }
            RadioButton {
                id:shadowBtn
                checked: false
                text: qsTr("Inner\nShadow")
                onClicked: {
                    zoom.visible = false
                    shadow.visible = true
                    hue.visible  = false
                }
            }

            RadioButton {
                id:hueBtn
                checked: false
                text: qsTr("Hue\nSaturation ")
                onClicked: {
                    zoom.visible = false
                    shadow.visible = false
                    hue.visible  = true
                }
            }
        }

        ColumnLayout{
            id: zoom
            Layout.alignment: Qt.AlignCenter
            anchors.top: rowRadioPhoto.bottom
            anchors.topMargin: 10
            Layout.row: 2
            Layout.fillHeight: true
            Layout.fillWidth: true
            Image {
                id: zoomDog
                source: "1.jpg"
                fillMode: Image.PreserveAspectFit
                width: parent.width
                height: parent.parent.height - zoomSettings.height - 80
                visible: false
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: horizontalOffsetText.top
            }

            ZoomBlur {
                id: zoomMask
                anchors.fill: zoomDog
                source: zoomDog
                //samples: samplesSlider.value
                samples: 24
                length: lengthSlider.value
                horizontalOffset: horizontalOffsetSlider.value
                verticalOffset: verticalOffsetSlider.value
                Layout.fillHeight: true
                Layout.maximumHeight: parent.height * 2
            }
            ColumnLayout{
                id: zoomSettings
                Layout.fillWidth: true
                Layout.fillHeight: true
                //anchors.bottom: parent.bottom
                Button{
                    id: saveZoomBtn
                    Layout.alignment: Qt.AlignCenter
                    Layout.minimumWidth: 100
                    Layout.minimumHeight: 40
                    Layout.fillHeight: true
                    Layout.column: 0
                    background: Rectangle {
                        width: parent.width
                        height: parent.height
                        color: saveZoomBtn.pressed ?  "#DFE1E5": "#F8F9FA"
                        radius: 4
                    }
                Text {
                    text: qsTr("Сохранить")
                    anchors.centerIn: parent
                    font.family: 'Arial'
                    font.pixelSize: 14

                }
                onClicked: saveFileDialog.open()
                }
                FileDialog {
                    id: saveFileDialog
                    title: "Save file"
                    nameFilters: ["Image files (*.png)", "All files (*)"]
                    selectExisting: false
                    onAccepted:
                    {
                        zoomMask.grabToImage(function(result) {
                            var url = (saveFileDialog.fileUrl.toString()+"").replace('file:///', '');
                            console.log(url);
                            result.saveToFile(url);
                       });
                    }
                }
                Text{
                    id: horizontalOffsetText
                    text: qsTr("horizontalOffset")
                    font.family: 'Arial'
                    font.pixelSize: 14
                    wrapMode: Text.Wrap
                    Layout.fillWidth: true
                }

                Slider{
                    id: horizontalOffsetSlider
                    anchors.top: horizontalOffsetText.bottom
                    Layout.fillWidth: true
                    from: -100.0
                    to: 100.0
                    value: 0
                    stepSize: 0.1
                    background: Rectangle {
                        x: horizontalOffsetSlider.leftPadding
                        y: horizontalOffsetSlider.topPadding + horizontalOffsetSlider.availableHeight / 2 - height / 2
                        implicitHeight: 6
                        width: horizontalOffsetSlider.availableWidth
                        height: implicitHeight
                        radius: 3
                        color: "#D1D4D6"

                        Rectangle {
                            width: horizontalOffsetSlider.visualPosition * parent.width
                            height: parent.height
                            color: "#4285F4"
                            radius: 3
                        }
                    }

                    handle: Rectangle {
                        x: horizontalOffsetSlider.leftPadding + horizontalOffsetSlider.visualPosition * (horizontalOffsetSlider.availableWidth - width)
                        y: horizontalOffsetSlider.topPadding + horizontalOffsetSlider.availableHeight / 2 - height / 2
                        implicitWidth: 20
                        implicitHeight: 20
                        radius: 10
                        color: horizontalOffsetSlider.pressed ? "#EDEFF4": "#ffffff"
                        border.color: "#DFE1E5"
                    }
                }

                Text{
                    id:  verticalOffsetText
                    text: qsTr("verticalOffset")
                    font.family: 'Arial'
                    font.pixelSize: 14
                    wrapMode: Text.Wrap
                    Layout.fillWidth: true
                }

                Slider{
                    id: verticalOffsetSlider
                    anchors.top: verticalOffsetText.bottom
                    Layout.fillWidth: true
                    from: -100.0
                    to: 100.0
                    value: 0
                    stepSize: 0.1
                    background: Rectangle {
                        x: verticalOffsetSlider.leftPadding
                        y: verticalOffsetSlider.topPadding + verticalOffsetSlider.availableHeight / 2 - height / 2
                        implicitHeight: 6
                        width: verticalOffsetSlider.availableWidth
                        height: implicitHeight
                        radius: 3
                        color: "#D1D4D6"

                        Rectangle {
                            width: verticalOffsetSlider.visualPosition * parent.width
                            height: parent.height
                            color: "#4285F4"
                            radius: 3
                        }
                    }

                    handle: Rectangle {
                        x: verticalOffsetSlider.leftPadding + verticalOffsetSlider.visualPosition * (verticalOffsetSlider.availableWidth - width)
                        y: verticalOffsetSlider.topPadding + verticalOffsetSlider.availableHeight / 2 - height / 2
                        implicitWidth: 20
                        implicitHeight: 20
                        radius: 10
                        color: verticalOffsetSlider.pressed ? "#EDEFF4": "#ffffff"
                        border.color: "#DFE1E5"
                    }
                }

                Text{
                    id:  lengthText
                    text: qsTr("length")
                    font.family: 'Arial'
                    font.pixelSize: 14
                    wrapMode: Text.Wrap
                    Layout.fillWidth: true
                }

                Slider{
                    id: lengthSlider
                    anchors.top: lengthText.bottom
                    Layout.fillWidth: true
                    from: 0
                    to: 100.0
                    value: 0
                    stepSize: 0.1
                    background: Rectangle {
                        x: lengthSlider.leftPadding
                        y: lengthSlider.topPadding + lengthSlider.availableHeight / 2 - height / 2
                        implicitHeight: 6
                        width: lengthSlider.availableWidth
                        height: implicitHeight
                        radius: 3
                        color: "#D1D4D6"

                        Rectangle {
                            width: lengthSlider.visualPosition * parent.width
                            height: parent.height
                            color: "#4285F4"
                            radius: 3
                        }
                    }

                    handle: Rectangle {
                        x: lengthSlider.leftPadding + lengthSlider.visualPosition * (lengthSlider.availableWidth - width)
                        y: lengthSlider.topPadding + lengthSlider.availableHeight / 2 - height / 2
                        implicitWidth: 20
                        implicitHeight: 20
                        radius: 10
                        color: lengthSlider.pressed ? "#EDEFF4": "#ffffff"
                        border.color: "#DFE1E5"
                    }
                }
            }
        }
        ColumnLayout{
            id: shadow
            Layout.alignment: Qt.AlignCenter
            anchors.top: rowRadioPhoto.bottom
            anchors.topMargin: 10
            Layout.row: 2
            Layout.fillHeight: true
            Layout.fillWidth: true
            visible: false
            Image {
                id: shadowDog
                source: "googleBig.png"
                fillMode: Image.PreserveAspectFit
                width: parent.width
                visible: false
                height: parent.parent.height - shadowSettings.height - 80
            }
            InnerShadow {
                id:shadowMask
                anchors.fill: shadowDog
                source: shadowDog
                fast: false
                samples: 24
                //samples: samplesSlider.value
                spread: spreadSlider.value
                horizontalOffset: horizontalOffsetSlider2.value
                verticalOffset: verticalOffsetSlider2.value
                radius: radiusSlider.value
                color: '#000000'
            }

            ColumnLayout{
                id: shadowSettings
                Layout.fillWidth: true
                Layout.fillHeight: true
                anchors.top: shadowDog.bottom
                Button{
                    id: saveShadowBtn
                    Layout.alignment: Qt.AlignCenter
                    Layout.minimumWidth: 100
                    Layout.minimumHeight: 40
                    Layout.fillHeight: true
                    Layout.column: 0
                    background: Rectangle {
                        width: parent.width
                        height: parent.height
                        color: saveShadowBtn.pressed ?  "#DFE1E5": "#F8F9FA"
                        radius: 4
                    }
                Text {
                    text: qsTr("Сохранить")
                    anchors.centerIn: parent
                    font.family: 'Arial'
                    font.pixelSize: 14

                }
                onClicked: saveFileDialog2.open()
                }
                FileDialog {
                    id: saveFileDialog2
                    title: "Save file"
                    nameFilters: ["Image files (*.png)", "All files (*)"]
                    selectExisting: false
                    onAccepted:
                    {
                        shadowMask.grabToImage(function(result) {
                            var url = (saveFileDialog2.fileUrl.toString()+"").replace('file:///', '');
                            console.log(url);
                            result.saveToFile(url);
                       });
                    }
                }
                Text{
                    id: horizontalOffsetText2
                    text: qsTr("horizontalOffset")
                    font.family: 'Arial'
                    font.pixelSize: 14
                    wrapMode: Text.Wrap
                    Layout.fillWidth: true
                }

                Slider{
                    id: horizontalOffsetSlider2
                    anchors.top: horizontalOffsetText2.bottom

                    Layout.fillWidth: true
                    from: -100.0
                    to: 100.0
                    value: 0
                    stepSize: 0.1
                    background: Rectangle {
                        x: horizontalOffsetSlider2.leftPadding
                        y: horizontalOffsetSlider2.topPadding + horizontalOffsetSlider2.availableHeight / 2 - height / 2
                        implicitHeight: 6
                        width: horizontalOffsetSlider2.availableWidth
                        height: implicitHeight
                        radius: 3
                        color: "#D1D4D6"

                        Rectangle {
                            width: horizontalOffsetSlider2.visualPosition * parent.width
                            height: parent.height
                            color: "#4285F4"
                            radius: 3
                        }
                    }

                    handle: Rectangle {
                        x: horizontalOffsetSlider2.leftPadding + horizontalOffsetSlider2.visualPosition * (horizontalOffsetSlider2.availableWidth - width)
                        y: horizontalOffsetSlider2.topPadding + horizontalOffsetSlider2.availableHeight / 2 - height / 2
                        implicitWidth: 20
                        implicitHeight: 20
                        radius: 10
                        color: horizontalOffsetSlider2.pressed ? "#EDEFF4": "#ffffff"
                        border.color: "#DFE1E5"
                    }
                }

                Text{
                    id:  verticalOffsetText2
                    text: qsTr("verticalOffset")
                    font.family: 'Arial'
                    font.pixelSize: 14
                    wrapMode: Text.Wrap
                    Layout.fillWidth: true
                }

                Slider{
                    id: verticalOffsetSlider2
                    anchors.top: verticalOffsetText2.bottom

                    Layout.fillWidth: true
                    from: -100.0
                    to: 100.0
                    value: 0
                    stepSize: 0.1
                    background: Rectangle {
                        x: verticalOffsetSlider2.leftPadding
                        y: verticalOffsetSlider2.topPadding + verticalOffsetSlider2.availableHeight / 2 - height / 2
                        implicitHeight: 6
                        width: verticalOffsetSlider2.availableWidth
                        height: implicitHeight
                        radius: 3
                        color: "#D1D4D6"

                        Rectangle {
                            width: verticalOffsetSlider2.visualPosition * parent.width
                            height: parent.height
                            color: "#4285F4"
                            radius: 3
                        }
                    }

                    handle: Rectangle {
                        x: verticalOffsetSlider2.leftPadding + verticalOffsetSlider2.visualPosition * (verticalOffsetSlider2.availableWidth - width)
                        y: verticalOffsetSlider2.topPadding + verticalOffsetSlider2.availableHeight / 2 - height / 2
                        implicitWidth: 20
                        implicitHeight: 20
                        radius: 10
                        color: verticalOffsetSlider2.pressed ? "#EDEFF4": "#ffffff"
                        border.color: "#DFE1E5"
                    }
                }

                Text{
                    id:  spreadText
                    text: qsTr("spread")
                    font.family: 'Arial'
                    font.pixelSize: 14
                    wrapMode: Text.Wrap
                    Layout.fillWidth: true
                }

                Slider{
                    id: spreadSlider
                    anchors.top: spreadText.bottom

                    Layout.fillWidth: true
                    from: 0
                    to: 1.0
                    value: 0.5
                    stepSize: 0.01
                    background: Rectangle {
                        x: spreadSlider.leftPadding
                        y: spreadSlider.topPadding + spreadSlider.availableHeight / 2 - height / 2
                        implicitHeight: 6
                        width: spreadSlider.availableWidth
                        height: implicitHeight
                        radius: 3
                        color: "#D1D4D6"

                        Rectangle {
                            width: spreadSlider.visualPosition * parent.width
                            height: parent.height
                            color: "#4285F4"
                            radius: 3
                        }
                    }

                    handle: Rectangle {
                        x: spreadSlider.leftPadding + spreadSlider.visualPosition * (spreadSlider.availableWidth - width)
                        y: spreadSlider.topPadding + spreadSlider.availableHeight / 2 - height / 2
                        implicitWidth: 20
                        implicitHeight: 20
                        radius: 10
                        color: spreadSlider.pressed ? "#EDEFF4": "#ffffff"
                        border.color: "#DFE1E5"
                    }
                }

                Text{
                    id:  radiusText
                    text: qsTr("radius")
                    font.family: 'Arial'
                    font.pixelSize: 14
                    wrapMode: Text.Wrap
                    Layout.fillWidth: true
                }

                Slider{
                    id: radiusSlider
                    anchors.top: radiusText.bottom

                    Layout.fillWidth: true
                    from: 0
                    to: 36
                    stepSize: 1
                    background: Rectangle {
                        x: radiusSlider.leftPadding
                        y: radiusSlider.topPadding + radiusSlider.availableHeight / 2 - height / 2
                        implicitHeight: 6
                        width: radiusSlider.availableWidth
                        height: implicitHeight
                        radius: 3
                        color: "#D1D4D6"

                        Rectangle {
                            width: radiusSlider.visualPosition * parent.width
                            height: parent.height
                            color: "#4285F4"
                            radius: 3
                        }
                    }

                    handle: Rectangle {
                        x: radiusSlider.leftPadding + radiusSlider.visualPosition * (radiusSlider.availableWidth - width)
                        y: radiusSlider.topPadding + radiusSlider.availableHeight / 2 - height / 2
                        implicitWidth: 20
                        implicitHeight: 20
                        radius: 10
                        color: radiusSlider.pressed ? "#EDEFF4": "#ffffff"
                        border.color: "#DFE1E5"
                    }
                }
            }
        }

        ColumnLayout{
            id: hue
            Layout.alignment: Qt.AlignCenter
            anchors.top: rowRadioPhoto.bottom
            anchors.topMargin: 10
            Layout.row: 2
            Layout.fillHeight: true
            Layout.fillWidth: true
            visible: false
            Image {
                id: hueDog
                source: "1.jpg"
                fillMode: Image.PreserveAspectFit
                width: parent.width
                visible: false
                height: parent.parent.height - hueSettings.height - 80
            }

            HueSaturation {
                id: hueMask
                anchors.fill: hueDog
                source: hueDog
                hue: hueSlider.value
                lightness: lightnessSlider.value
                saturation: saturationSlider.value
            }

            ColumnLayout{
                id: hueSettings
                Layout.fillWidth: true
                Layout.fillHeight: true
                anchors.top: hueDog.bottom
                Button{
                    id: saveHueBtn
                    Layout.alignment: Qt.AlignCenter
                    Layout.minimumWidth: 100
                    Layout.minimumHeight: 40
                    Layout.fillHeight: true
                    Layout.column: 0
                    background: Rectangle {
                        width: parent.width
                        height: parent.height
                        color: saveHueBtn.pressed ?  "#DFE1E5": "#F8F9FA"
                        radius: 4
                    }
                Text {
                    text: qsTr("Сохранить")
                    anchors.centerIn: parent
                    font.family: 'Arial'
                    font.pixelSize: 14

                }
                onClicked: saveFileDialog3.open()
                }
                FileDialog {
                    id: saveFileDialog3
                    title: "Save file"
                    nameFilters: ["Image files (*.png)", "All files (*)"]
                    selectExisting: false
                    onAccepted:
                    {
                        hueMask.grabToImage(function(result) {
                            var url = (saveFileDialog3.fileUrl.toString()+"").replace('file:///', '');
                            console.log(url);
                            result.saveToFile(url);
                       });
                    }
                }
                Text{
                    id: hueText
                    text: qsTr("hue")
                    font.family: 'Arial'
                    font.pixelSize: 14
                    wrapMode: Text.Wrap
                    Layout.fillWidth: true
                }

                Slider{
                    id: hueSlider
                    anchors.top: hueText.bottom

                    Layout.fillWidth: true
                    from: -1.0
                    to: 1.0
                    value: 0
                    stepSize: 0.01
                    background: Rectangle {
                        x: hueSlider.leftPadding
                        y: hueSlider.topPadding + hueSlider.availableHeight / 2 - height / 2
                        implicitHeight: 6
                        width: hueSlider.availableWidth
                        height: implicitHeight
                        radius: 3
                        color: "#D1D4D6"

                        Rectangle {
                            width: hueSlider.visualPosition * parent.width
                            height: parent.height
                            color: "#4285F4"
                            radius: 3
                        }
                    }

                    handle: Rectangle {
                        x: hueSlider.leftPadding + hueSlider.visualPosition * (hueSlider.availableWidth - width)
                        y: hueSlider.topPadding + hueSlider.availableHeight / 2 - height / 2
                        implicitWidth: 20
                        implicitHeight: 20
                        radius: 10
                        color: hueSlider.pressed ? "#EDEFF4": "#ffffff"
                        border.color: "#DFE1E5"
                    }
                }

                Text{
                    id:  lightnessText
                    text: qsTr("lightness")
                    font.family: 'Arial'
                    font.pixelSize: 14
                    wrapMode: Text.Wrap
                    Layout.fillWidth: true
                }

                Slider{
                    id: lightnessSlider
                    anchors.top: lightnessText.bottom
                    Layout.fillWidth: true
                    from: -1.0
                    to: 1.0
                    value: 0
                    stepSize: 0.01
                    background: Rectangle {
                        x: lightnessSlider.leftPadding
                        y: lightnessSlider.topPadding + lightnessSlider.availableHeight / 2 - height / 2
                        implicitHeight: 6
                        width: lightnessSlider.availableWidth
                        height: implicitHeight
                        radius: 3
                        color: "#D1D4D6"

                        Rectangle {
                            width: lightnessSlider.visualPosition * parent.width
                            height: parent.height
                            color: "#4285F4"
                            radius: 3
                        }
                    }

                    handle: Rectangle {
                        x: lightnessSlider.leftPadding + lightnessSlider.visualPosition * (lightnessSlider.availableWidth - width)
                        y: lightnessSlider.topPadding + lightnessSlider.availableHeight / 2 - height / 2
                        implicitWidth: 20
                        implicitHeight: 20
                        radius: 10
                        color: lightnessSlider.pressed ? "#EDEFF4": "#ffffff"
                        border.color: "#DFE1E5"
                    }
                }

                Text{
                    id:  saturationText
                    text: qsTr("saturation")
                    font.family: 'Arial'
                    font.pixelSize: 14
                    wrapMode: Text.Wrap
                    Layout.fillWidth: true
                }

                Slider{
                    id: saturationSlider
                    anchors.top: saturationText.bottom
                    Layout.fillWidth: true
                    from: -1.0
                    to: 1.0
                    value: 0
                    stepSize: 0.01
                    background: Rectangle {
                        x: saturationSlider.leftPadding
                        y: saturationSlider.topPadding + saturationSlider.availableHeight / 2 - height / 2
                        implicitHeight: 6
                        width: saturationSlider.availableWidth
                        height: implicitHeight
                        radius: 3
                        color: "#D1D4D6"

                        Rectangle {
                            width: saturationSlider.visualPosition * parent.width
                            height: parent.height
                            color: "#4285F4"
                            radius: 3
                        }
                    }
                    handle: Rectangle {
                        x: saturationSlider.leftPadding + saturationSlider.visualPosition * (saturationSlider.availableWidth - width)
                        y: saturationSlider.topPadding + saturationSlider.availableHeight / 2 - height / 2
                        implicitWidth: 20
                        implicitHeight: 20
                        radius: 10
                        color: saturationSlider.pressed ? "#EDEFF4": "#ffffff"
                        border.color: "#DFE1E5"
                    }
                }

            }
        }
    }
}
