
import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.3
import QtMultimedia 5.15
import QtGraphicalEffects 1.15

Page {
    id : page2GUI
    width: 600
    height: 900
    visible: true
    header:
        GridLayout{
        columnSpacing: 10
        anchors.rightMargin: 15
        anchors.bottomMargin: 0
        anchors.leftMargin: 15
        anchors.topMargin: 15
        Image {
            id: googleImg2
            source: "/unnamed.png"
            Layout.column: 1
            Layout.row: 1
        }
        Label {
            id: labLable2
            text: qsTr("ЛР2 Работа с видео")
            font.pixelSize: 20
            Layout.column: 2
            Layout.row: 1
            font.family: 'Arial'
        }
    }

    Rectangle{
        id: hr2
        color:"#DFE1E5"
        y:0
        height:1
        width: parent.width
    }
    GridLayout{
        anchors.fill: parent
        anchors.rightMargin: 15
        anchors.bottomMargin: 15
        anchors.leftMargin: 15
        anchors.topMargin: 15
        columnSpacing: 10
        rowSpacing: 10

        RowLayout{
            id:rowRadio
            Layout.alignment: Qt.AlignCenter
            Layout.columnSpan: 2
            Layout.row: 1
            anchors.top: parent.top
            RadioButton {
                id:vidBtn
                checked: true
                text: qsTr("Видео")
                onClicked: {
                    columnlayout1.visible = true;
                    //columnlayout2.visible = false
                    videoSlider.visible = true;
                    name.visible = true;
                    playBtn.visible = true;
                }
            }
            RadioButton {
                id:camBtn
                text: qsTr("Камера")
                onClicked: {
                    columnlayout1.visible = false
                    mediaplayer.playbackState = mediaplayer.pause();
                    time.restart();
                    time.stop();
                    //columnlayout2.visible = true
                }
            }
        }
        ColumnLayout{
            id:columnlayout1
            Layout.columnSpan: 2
            Layout.row: 2
            anchors.top: rowRadio.bottom
            //Layout.fillHeight: true
            MediaPlayer {
                id: mediaplayer
                source: "/cat.mp4"
                //playbackState: "StoppedState"
                loops:MediaPlayer.Infinite
            }

            Timer{
                id: time
                interval: 3000
                onTriggered: {
                videoSlider.visible = false
                name.visible = false
                playBtn.visible = false
                }
            }

            VideoOutput {
                Layout.fillWidth: true
                Layout.maximumHeight: parent.width / 3 * 2
                source: mediaplayer
                id:videooutput
                MouseArea{
                    id: playArea
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: {
                        playBtn.clicked();
                    }
                    onEntered: {
                        videoSlider.visible = true
                        name.visible = true
                        playBtn.visible = true
                        MediaPlayer.PlayingState == mediaplayer.playbackState ?  time.restart() : time.stop();
                    }
                }
            }

            Slider{
                Layout.fillWidth: true
                id:videoSlider
                to: mediaplayer.duration
                property bool sync: false
                onValueChanged: {
                    if (!sync){
                        mediaplayer.seek(value)
                        time.restart()
                    }
                }
                Connections{
                    target: mediaplayer
                    onPositionChanged: {
                        videoSlider.sync = true
                        videoSlider.value = mediaplayer.position
                        videoSlider.sync = false
                    }
                }
                background: Rectangle {
                    x: videoSlider.leftPadding
                    y: videoSlider.topPadding + videoSlider.availableHeight / 2 - height / 2
                    implicitHeight: 6
                    width: videoSlider.availableWidth
                    height: implicitHeight
                    radius: 3
                    color: "#D1D4D6"

                    Rectangle {
                        width: videoSlider.visualPosition * parent.width
                        height: parent.height
                        color: "#4285F4"
                        radius: 3
                    }
                }

                handle: Rectangle {
                    x: videoSlider.leftPadding + videoSlider.visualPosition * (videoSlider.availableWidth - width)
                    y: videoSlider.topPadding + videoSlider.availableHeight / 2 - height / 2
                    implicitWidth: 20
                    implicitHeight: 20
                    radius: 10
                    color: videoSlider.pressed ? "#EDEFF4": "#ffffff"
                    border.color: "#DFE1E5"
                }
            }
            Text {
                id: name
                text: Qt.formatTime(new Date(mediaplayer.position), "mm:ss")
                anchors.horizontalCenter: parent.horizontalCenter
                height: 10
            }

            Button {
                id: playBtn
                Layout.minimumWidth: 50
                Layout.minimumHeight: 30
                anchors.horizontalCenter: parent.horizontalCenter

                background: Rectangle {
                    width: parent.width
                    height: parent.height
                    color: playBtn.pressed ?  "#DFE1E5": "#F8F9FA"
                    radius: 4
                }

                onClicked: {
                    videoSlider.visible = true;
                    name.visible = true;
                    playBtn.visible = true;
                    MediaPlayer.PlayingState == mediaplayer.playbackState ?  mediaplayer.pause() : mediaplayer.play();
                    MediaPlayer.PlayingState == mediaplayer.playbackState ?  time.restart() : time.stop();
                }

                Image {
                    Layout.alignment: Qt.AlignCenter
                    id: playPauseBtn
                    anchors.centerIn: parent
                    width: parent.height - 10
                    height: parent.height - 10
                    source: MediaPlayer.PlayingState == mediaplayer.playbackState ? "/pause.png"  : "/play.png"
                }
            }
        }

        ColumnLayout{
            id:columnlayout2
            Layout.columnSpan: 2
            Layout.row: 2
            anchors.top: rowRadio.bottom
            visible:{
                if (camBtn.checked == true){
                    camera.start()
                    return true
                }
                else{
                    camera.stop()
                    return false
                }
            }
            Camera{
                id: camera
                videoRecorder.audioEncodingMode: CameraRecorder.ConstantBitrateEncoding;
                videoRecorder.audioBitRate: 128000
                videoRecorder.mediaContainer: "mp4"
            }
            VideoOutput{
                id: camOutput
                source: camera
                Layout.fillWidth: true
                Layout.fillHeight: true
                autoOrientation: true
            }

            RowLayout{
                Layout.alignment: Qt.AlignCenter
                Layout.fillWidth: true
                Layout.row: 3
                Button{
                    id:photoBtn
                    onClicked: camera.imageCapture.capture()
                    Layout.minimumWidth: 100
                    Layout.minimumHeight: 60
                    background: Rectangle {
                        width: parent.width
                        height: parent.height
                        color: photoBtn.pressed ?  "#DFE1E5": "#F8F9FA"
                        radius: 4
                    }

                    Image {
                        Layout.alignment: Qt.AlignCenter
                        anchors.centerIn: parent
                        width: parent.height - 10
                        height: parent.height - 10
                        source:"/photo.png"
                    }
                }
                Button{
                    id:videoBtn
                    Layout.minimumWidth: 100
                    Layout.minimumHeight: 60

                    Image {
                        Layout.alignment: Qt.AlignCenter
                        anchors.centerIn: parent
                        width: parent.height - 10
                        height: parent.height - 10
                        source: {
                            if (camera.videoRecorder.recorderStatus == CameraRecorder.RecordingStatus){
                                return "/record.png"
                            }
                            else{
                                return '/video.png'
                            }
                        }
                    }

                    onClicked: {
                        if(camera.videoRecorder.recorderState == CameraRecorder.StoppedState){
                            camera.videoRecorder.outputLocation = "C:/Users/super/Documents/191_331_Mamonova_1/video";
                            camera.videoRecorder.record()
                        }
                        else
                            camera.videoRecorder.stop()
                    }

                    background: Rectangle {
                        width: parent.width
                        height: parent.height
                        color: videoBtn.pressed ?  "#DFE1E5": "#F8F9FA"
                        radius: 4
                    }
                }
            }
        }
    }
}
